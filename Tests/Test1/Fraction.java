public class Fraction {
    private int a;
    private int b;

    public Fraction() {
        this.a = 0;
        this.b = 1;
    }

    public Fraction(int a, int b) {
        this.setA(a);
        this.setB(b);
    }

    public void setB(int b) {
        if (b > 0) {
            this.b = b;
        } else this.b = 1;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public void print() {
        System.out.println(a + "/" + b);
    }

    public void optimize() {
        int temp = gcd(this.a, this.b);
        this.a = this.a / temp;
        this.b = this.b / temp;
    }

    private static int gcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

    public void mult(Fraction d1, Fraction d2) {
        this.a = d1.a * d2.a;
        this.b = d1.b * d2.b;
    }
}
