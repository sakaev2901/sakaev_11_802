
public class Main {
    public static void main(String[] args) {
        Fraction frac1= new Fraction(4, 6);
        Fraction frac2= new Fraction(5, 6);
        Fraction frac3= new Fraction();
        frac1.print();
        frac1.optimize();
        frac1.print();
        frac3.mult(frac1, frac2);
        frac3.print();
    }
}
