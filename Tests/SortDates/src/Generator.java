import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Generator {
    public static final int COUNT_OF_LINES = 1000000;
    public static void generate() throws IOException {
        BufferedWriter bufferedReader  = new BufferedWriter(new FileWriter("out.txt"));
        for (int i = 0; i < COUNT_OF_LINES; i++) {
//            int countOfchars = 100 + (int)(Math.random() * 10000);
//            String text = "";
//            for (int j = 0; j < countOfchars; j++) {
//                char c = (char)((int)'a' + (int)(Math.random() * 25));
//                text += c;
//            }
//            bufferedReader.write(text);
//            bufferedReader.newLine();
//            if ((int)(Math.random() * 10) % 2 == 0) {
//                int length = 2 +  (int)(Math.random() * 5);
//                int index = (int)(Math.random() * (countOfchars - length - 1));
//                bufferedReader.write(text.substring(index, index + length));
//                bufferedReader.newLine();
//            } else {
//                bufferedReader.write("aaaaa");
//                bufferedReader.newLine();
//            }
            int year = 1970 + (int)(Math.random() * 18231);
            int day = 1 + (int)(Math.random() * 31);
            int month = 1 + (int)(Math.random() * 12);
            if(year % 4 == 0) {
                if (month == 2) {
                    day = 29;
                } else {
                    day = 28;
                }
            }
            switch (month) {
                case 1: day = 1 + (int)(Math.random() * 31);
                case 3: day = 1 + (int)(Math.random() * 31);
                case 4: day = 1 + (int)(Math.random() * 30);
                case 5: day = 1 + (int)(Math.random() * 31);
                case 6: day = 1 + (int)(Math.random() * 30);
                case 7: day = 1 + (int)(Math.random() * 31);
                case 8: day = 1 + (int)(Math.random() * 31);
                case 9: day = 1 + (int)(Math.random() * 30);
                case 10: day = 1 + (int)(Math.random() * 31);
                case 11: day = 1 + (int)(Math.random() * 30);
                case 12: day = 1 + (int)(Math.random() * 31);
            }
            bufferedReader.write(year + "-" + month + "-" + day);
            bufferedReader.newLine();
        }
        bufferedReader.close();
    }
}
