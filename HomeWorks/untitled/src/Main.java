
public class Main {

    public static void main(String[] args) {
        Runnable task = () -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println(Thread.currentThread().getName() + " " + i);
            }
        };

        ExecutorService executorService = new ExecutorService(5);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
        executorService.execute(task);
    }
}
