import java.util.Scanner;


public class program {

    public static double f(double x) {
        return x * x;
    }

    public static double left(double a, double b, int n) {
        double h = Math.abs(a - b) / n;
        double result = 0;
        for (double i = a; b - i > 0.000001; i += h) {
            result += h * f(i);
        }
        return result;
    }

    public static double right(double a, double b, int n) {
        double h = Math.abs(a - b) / n;
        double result = 0;
        for (double i = a + h; i <= b; i += h) {
            result += h * f(i);
        }
        return result;
    }

    public static double mid(double a, double b, int n) {
        double h = Math.abs(a - b) / n;
        double result = 0;
        for (double i = a + h / 2; i <= b; i += h) {
            result += h * f(i);
        }
        return result;
    }

    public static double trapec(double a, double b, int n) {
        double h = Math.abs(a - b) / n;
        double result = 0;
        for (double i = a; i <= b; i += h) {
            result += h * (f(i) + f(i - h)) / 2;
        }
        return result;
    }

    public static double Simpson(double a, double b, int n) {
        double h = Math.abs(a - b) / n;
        double result = 0;
        for (double i = a; b - i > 0.0000001; i += h) {
            result += (h / 6) * (f(i) + 4 * f(i + h / 2) + f(i + h));
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a;
        double b;
        int n;
        while (true) {
            System.out.println("Меню: ");
            System.out.println("1.  Метод левых прямоугольников");
            System.out.println("2.  Метод правых прямоугольников");
            System.out.println("3.  Метод трапеций");
            System.out.println("4.  Формула Симпсона");
            System.out.println("5.  Выход");
            int x = scanner.nextInt();
            switch (x) {
                case 1: {
                    System.out.print("Введите левую границу: ");
                    a = scanner.nextDouble();
                    System.out.print("Введите правую границу: ");
                    b = scanner.nextDouble();
                    System.out.print("Введите количество итерация: ");
                    n = scanner.nextInt();
                    System.out.println("Метод левых прямоугольников: ");
                    System.out.print("   количество итераций - ");
                    System.out.println(n);
                    System.out.print("   знчаение интеграла ");
                    System.out.println(left(a, b, n));
                    System.out.println();
                }
                break;
                case 2: {
                    System.out.print("Введите левую границу: ");
                    a = scanner.nextDouble();
                    System.out.print("Введите правую границу: ");
                    b = scanner.nextDouble();
                    System.out.print("Введите количество итерация: ");
                    n = scanner.nextInt();
                    System.out.println("Метод правых прямоугольников: ");
                    System.out.print("   количество итераций - ");
                    System.out.println(n);
                    System.out.print("   знчаение интеграла ");
                    System.out.println(right(a, b, n));
                    System.out.println();
                }
                break;
                case 3: {
                    System.out.print("Введите левую границу: ");
                    a = scanner.nextDouble();
                    System.out.print("Введите правую границу: ");
                    b = scanner.nextDouble();
                    System.out.print("Введите количество итерация: ");
                    n = scanner.nextInt();
                    System.out.println("Метод трапеции: ");
                    System.out.print("   количество итераций - ");
                    System.out.println(n);
                    System.out.print("   знчаение интеграла ");
                    System.out.println(trapec(a, b, n));
                    System.out.println();
                }
                break;
                case 4: {
                    System.out.print("Введите левую границу: ");
                    a = scanner.nextDouble();
                    System.out.print("Введите правую границу: ");
                    b = scanner.nextDouble();
                    System.out.print("Введите количество итерация: ");
                    n = scanner.nextInt();
                    System.out.println("Формула Симпсона: ");
                    System.out.print("   количество итераций - ");
                    System.out.println(n);
                    System.out.print("   знчаение интеграла ");
                    System.out.println(Simpson(a, b, n));
                }
                break;
                case 5: {
                    System.exit(0);
                }
                default: {
                    System.out.println("Неизвестное значение. Введите заново");
                }
                break;
            }
        }
    }
}

