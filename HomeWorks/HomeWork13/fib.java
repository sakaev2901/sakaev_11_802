import java.util.Scanner;

public class fib {

    public static int fibonachi(int i, int j, int k, int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        if (k < n) {
            return fibonachi(j, i + j, ++k, n);
        } else {
            return i + j;
        }
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(fibonachi(0, 1, 2, n));
    }
}
