
import java.io.*;
import java.util.HashMap;

public class UserReader {
    public static void read(HashMap<Long, User> hashMap) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("users.txt"));
            for (int i = 0; i < 6; i++) {
                String[] strings = bufferedReader.readLine().split(" ");
                long id = Integer.parseInt(strings[0]);
                String userName = strings[1];
                boolean gender = Boolean.parseBoolean(strings[2]);
                hashMap.put(id, new User(id, gender, userName));

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
