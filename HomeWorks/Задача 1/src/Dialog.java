import java.util.LinkedList;

public class Dialog {
    User user1;
    User user2;
    LinkedList<Message> dialog;

    public Dialog(User user1, User user2) {
        this.user1 = user1;
        this.user2 = user2;
        dialog = new LinkedList<>();
    }

    public void putMessage(Message message) {
        dialog.add(message);
    }
}
