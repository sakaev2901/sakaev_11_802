
import java.util.*;

public class Main {

    public static void find(int id1, int id2, LinkedList<Dialog> dialogs,  HashMap<Long, User> users) {
        Iterator iterator = dialogs.iterator();
        while (iterator.hasNext()) {
            Dialog temp = (Dialog) iterator.next();
            User sender = users.get(id1);
            User receiver = users.get(id2);
            if (temp.user1 == sender && temp.user2 == receiver || temp.user2 == sender && temp.user1 == receiver) {
                Iterator iterator1 = temp.dialog.iterator();
                while (iterator1.hasNext()) {
                    Message message = (Message) iterator1.next();
                    System.out.println(message.senderId + " " + message.text);
                }
            }
        }
    }

    public static void main(String[] args) {
        HashMap<Long, User> users = new HashMap<>();
        UserReader.read(users);
        UserReader.read(users);
        LinkedList<Dialog> dialogs = new LinkedList<>();
        MessageReader.read(users, dialogs);
        long id1 = 1;
        long id2 = 2;
        find(1, 2, dialogs, users);
    }
}
