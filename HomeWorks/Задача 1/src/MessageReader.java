import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

public class MessageReader {
    public static void read(HashMap<Long, User> hashMap, LinkedList<Dialog> linkedList) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("messages.txt"));
            for (int i = 0; i < 2; i++) {
                String[] strings = bufferedReader.readLine().split(" ");
                User sender = hashMap.get(Integer.parseInt(strings[0]));
                User receiver = hashMap.get(Integer.parseInt(strings[1]));
                String mess = strings[2];
                boolean status = Boolean.parseBoolean(strings[3]);
                Iterator iterator = linkedList.iterator();
                Message message = new Message(sender, receiver, mess, status);
                Boolean isExist = false;
                while (iterator.hasNext()) {
                    Dialog temp = (Dialog) iterator.next();
                    if (temp.user1 == sender && temp.user2 == receiver || temp.user2 == sender && temp.user1 == receiver) {
                        temp.putMessage(message);
                        isExist = true;
                    }
                }
                if (!isExist) {
                    linkedList.add(new Dialog(sender, receiver));
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
