public class User {
    long id;
    boolean gender;
    String username;

    public User(long id, boolean gender, String username) {
        this.id = id;
        this.gender = gender;
        this.username = username;
    }
}
