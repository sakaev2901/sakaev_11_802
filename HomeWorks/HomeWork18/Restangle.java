public class Restangle extends Figures{
   private double length;
   private double width;

   public Restangle(double x, double y, String name, double length, double width) {
       super(x, y, name);
       this.length = length;
       this.width = width;
   }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public void calculateArea() {
        this.area = this.length * this.width;
    }

    @Override
    public void calculatePerimetr() {
        this.perimetr = 2 * (this.width + this.length);
    }

    @Override
    public void printArea() {
        System.out.println(this.name + " " + this.area);
    }

    @Override
    public void printPerimetr() {
        System.out.println(this.name +" " + this.perimetr);
    }

    @Override
    public void printCentre() {
        System.out.println(this.name + " " + this.x + " " + this.y);
    }
}
