public class Circle extends Figures {
    private static final double PI = 3.14;
    private double radius;

    public Circle(double x, double y, String name, double radius) {
        super(x, y, name);
        this.radius = radius;
    }

    @Override
    public void calculateArea() {
        this.area = PI * this.radius * this.radius;
    }

    @Override
    public void calculatePerimetr() {
        this.perimetr = 2 *  PI * this.radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public void printArea() {
        System.out.println(this.name + " " + this.area);
    }

    @Override
    public void printPerimetr() {
        System.out.println(this.name +" " + this.perimetr);
    }

    @Override
    public void printCentre() {
        System.out.println(this.name + " " + this.x + " " + this.y);
    }
}
