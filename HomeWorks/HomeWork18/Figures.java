public abstract class  Figures {
    protected double x;
    protected double y;
    protected String name;
    protected double area;
    protected double perimetr;

    public Figures(double x, double y, String name) {
          this.x = x;
          this.y = y;
          this.name = name;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public abstract void calculateArea();

    public abstract void calculatePerimetr();

    public abstract void printArea();

    public abstract void printPerimetr();

    public abstract void printCentre();
}
