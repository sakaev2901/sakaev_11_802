public class Square extends Figures {
    private double length;

    public Square(double x, double y, String name, double length) {
        super(x, y, name);
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    @Override
    public void calculateArea() {
        this.area = this.length * this.length;
    }

    @Override
    public void calculatePerimetr() {
        this.perimetr = 4 * length;
    }

    @Override
    public void printArea() {
        System.out.println(this.name + " " + this.area);
    }

    @Override
    public void printPerimetr() {
        System.out.println(this.name +" " + this.perimetr);
    }

    @Override
    public void printCentre() {
        System.out.println(this.name + " " + this.x + " " + this.y);
    }
 }
