public class Trapezium extends Figures{
    private double topBase;
    private double bottomBase;
    private double height;
    private double leftSide;
    private double rightSide;

    public Trapezium(double x, double y, String name, double topBase, double bottomBase, double height, double leftSide, double rightSide) {
        super(x, y, name);
        this.topBase = topBase;
        this.bottomBase = bottomBase;
        this.height = height;
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    public double getTopBase() {
        return topBase;
    }

    public double getBottomBase() {
        return bottomBase;
    }

    public double getHeight() {
        return height;
    }

    public double getLeftSide() {
        return leftSide;
    }

    public double getRightSide() {
        return rightSide;
    }

    @Override
    public void calculateArea() {
        this.area = (this.topBase + this.bottomBase) * this.height / 2;
    }

    @Override
    public void calculatePerimetr() {
        this.perimetr = this.bottomBase + this.rightSide + this.leftSide + this.topBase;
    }

    @Override
    public void printArea() {
        System.out.println(this.name + " " + this.area);
    }

    @Override
    public void printPerimetr() {
        System.out.println(this.name +" " + this.perimetr);
    }

    @Override
    public void printCentre() {
        System.out.println(this.name + " " + this.x + " " + this.y);
    }
}
