
import java.util.*;

public class Main {

    public static int checker(int x) {
        int sum1 = 0;
        int mult1 = 1;
        while (x > 0) {
            int mod = x % 10;
            sum1 += mod;
            mult1 *= mod;
            x /= 10;
        }
        return Math.abs(mult1 - sum1);
    }
    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(-123);
        list.add(124);
        list.add(125);
        list.add(126);
        list.add(127);
        list.add(129);
        list.add(128);
        Collections.sort(list, (x, y) -> -1 * (checker(Math.abs(x)) - checker(Math.abs(y))));
        System.out.println(list);
    }
}
