import java.util.Scanner;

public class program1 {

    public static void Posledovatelnost(int n) {
        if (n == 1) {
            System.out.print(1 + " ");
        } else if (n > 1) {
            Posledovatelnost(n - 1);
            System.out.print(n + " ");
        }

    }

    public static void aToB(int a, int b) {
        if (b == a) {
            System.out.print(a + " ");
        } else if (b > a) {
            aToB(a, b - 1);
            System.out.print(b + " ");
        }
    }

    public static int akkerman(int m, int n) {
        if (m == 0) {
            return n + 1;
        } else if (n == 0 && m > 0) {
            return akkerman(m - 1, 1);
        } else {
            return akkerman(m - 1, akkerman(m, n - 1));
        }
    }

    public static String powOf3(int n, int mod) {
        if (mod > 0) {
            return "NO";
        } else if (n == 1 && mod == 0) {
            return "YES";
        }
        return powOf3(n / 3, mod = n % 3);
    }

    public static int sumOfDigits(int n) {
        if (n == 0) {
            return 0;
        }
        return sumOfDigits(n / 10) + n % 10;
    }

    public static String numFromRight(int n) {
        if (n < 10) {
            return " " + n;
        } else {
            return " " + n % 10 + numFromRight(n / 10);
        }
    }

    public static String numFromLeft(int n) {
        if (n < 10) {
            return " " + n;
        } else {
            return numFromLeft(n / 10) + " " + n % 10;
        }
    }

    public static String primeNumber(int n, int k) {
        if (n < 2) {
            return "NO";
        } else if (n == 2) {
            return "YES";
        } else if (n % k == 0) {
            return "NO";
        } else if (k < Math.sqrt(n)) {
            return primeNumber(n, k + 1);
        } else {
            return "YES";
        }
    }

    public static void primeMembers(int n, int k, int t) {
        if (n > 0) {
            if (k <= Math.sqrt(t)) {
                if (n % k == 0) {
                    System.out.println(k);
                    primeMembers(n / k, k, t);
                } else {
                    primeMembers(n, k + 1, t);
                }
            }
        }
    }

    public static String Polindrom(char m[], int i, int j) {
        if (m[i] != m[j]) {
            return "NO";
        } else if (i > j) {
            return "YES";
        } else {
            return Polindrom(m, i + 1, j - 1);
        }
    }

    public static void odd() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n != 0) {
            if (n % 2 == 1) {
                System.out.println(n);
                odd();
            } else {
                odd();
            }
        }
    }

    public static void oddIndex() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = 1;
        if (n != 0) {
            k = sc.nextInt();
        }
        if (n != 0 && k != 0) {
            System.out.println(n);
            oddIndex();
        } else if (k == 0) {
            System.out.println(n);
        }
    }

    public static int maxOfSeq() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n == 0) {
            return 0;
        } else {
            return Math.max(n, maxOfSeq());
            }
    }

    public static double midOfSeq(double sum, int i) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n != 0) {
            return midOfSeq(sum + n, i + 1);
        } else {
            return sum / i;
        }
    }

    public static int secondMaxOfSeq(int max1, int max2) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n != 0) {
            if (n > max1) {
                return secondMaxOfSeq(n, max1);
            } else if (n > max2) {
                return secondMaxOfSeq(max1, n);
            }
        }
        return max2;
    }

    public static int countOfMax(int max, int count) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n != 0) {
            if (n > max) {
                return countOfMax(n, 1);
            } else if (n == max) {
                return countOfMax(n, count + 1);
            } else {
                return countOfMax(max, count);
            }
        }
        return count;
    }

    public static void seqOfDelta(int n, int k) {
        if (n > 0) {
            for (int i = 1; i <= k && i <= n; i++) {
                System.out.print(k + " ");
            }
            seqOfDelta(n - k, k + 1);
        }
    }

    public static int countOfSum(int sum, int k) {
        if (9 <= sum)
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("1. Posledovatelnost.");
            System.out.println("2. A to B.");
            System.out.println("3. Akkerman.");
            System.out.println("4. pow of 3.");
            System.out.println("5. sum of digits.");
            System.out.println("6. num of right.");
            System.out.println("7. num from left.");
            System.out.println("8. prime number");
            System.out.println("9. prime members");
            System.out.println("10. polindrom");
            System.out.println("11. odd");
            System.out.println("12. odd index.");
            System.out.println("13. Max of sequence.");
            System.out.println("14. Mid of sequence.");
            System.out.println("15. Second Max of sequence.");
            System.out.println("16. Count of max.");
            System.out.println("17. Cout of one.");
            System.out.println("18. Sequence of delta.");
            System.out.println("19. Count of sum.");
            int x = sc.nextInt();
            switch (x) {
                case 1: {
                    int n = sc.nextInt();
                    Posledovatelnost(n);
                    System.out.println();
                }
                break;
                case 2: {
                    int a = sc.nextInt();
                    int b = sc.nextInt();
                    if (a < b) {
                        aToB(a, b);
                    } else {
                        aToB(b, a);
                    }
                    System.out.println();
                }
                break;
                case 3: {
                    Scanner scanner = new Scanner(System.in);
                    int n = scanner.nextInt();
                    int m = scanner.nextInt();
                    System.out.println(akkerman(n, m));
                }
                case 4: {
                    int n = sc.nextInt();
                    int mod = 0;
                    System.out.println(powOf3(n, mod));
                }
                break;
                case 5: {
                    int n = sc.nextInt();
                    System.out.println(sumOfDigits(n));
                }
                break;
                case 6: {
                    int n = sc.nextInt();
                    System.out.println(numFromRight(n));
                }
                break;
                case 7: {
                    int n = sc.nextInt();
                    System.out.println(numFromLeft(n));
                }
                break;
                case 8: {
                    int n = sc.nextInt();
                    int k = 2;
                    System.out.println(primeNumber(n, k));
                }
                break;
                case 9: {
                    int n = sc.nextInt();
                    int k = 2;
                    int t = n;
                    primeMembers(n, k, t);
                }
                break;
                case 10: {
                    String s = sc.next();
                    int n = s.length();
                    char m[] = new char[n];
                    m = s.toCharArray();
                    int i = 0;
                    int j = s.length() - 1;
                    System.out.println(Polindrom(m, i, j));
                }
                break;
                case 11: {
                    odd();
                }
                break;
                case 12: {
                    oddIndex();
                }
                break;
                case 13: {
                    System.out.println(maxOfSeq());
                }
                break;
                case 14: {
                    System.out.println(midOfSeq(0, 0));
                }
                break;
                case 15: {
                    System.out.println(secondMaxOfSeq(0, 0));
                }
                break;
                case 16: {
                    System.out.println(countOfMax(0, 0));
                }
                break;
                case 17: {
                    System.out.println();
                }
                break;
                case 18: {
                    int n = sc.nextInt();
                    seqOfDelta(n, 1);
                }
            }
        }
    }
}