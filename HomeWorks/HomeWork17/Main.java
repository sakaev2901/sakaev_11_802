import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s1 = "Привет, как дела? ЧТО НОВОГО? А у меня ничего.";
        String s2 = "Ну, привет. Тоже ничего. Дела делишки, дела. Привет";
        PlagiatCheck string1 = new PlagiatCheck(s1);
        PlagiatCheck string2 = new PlagiatCheck(s2);
        PlagiatCheck string3 = new PlagiatCheck(s1);
        string1.editingOfLength();
        string1.sort();
        string1.countOfElements();
        string2.editingOfLength();
        string2.sort();
        string2.countOfElements();
        string3.mergeSort(string1, string2);
        string3.countOfElements();
        string1.makeVector(string3);
        string2.makeVector(string3);
        string1.check(string2);
        string1.printPercent();
    }
}