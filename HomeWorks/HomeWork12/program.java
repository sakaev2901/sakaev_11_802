import java.util.Arrays;
import java.util.Scanner;

class program {

    public static void toLower(char text[]) {
        int left = 'A';
        int right = 'Z';
        for (int i = 0; i < text.length; i++) {
            if (text[i] >= left && text[i] <= right) {
                text[i] = (char) (text[i] + 32);
            }
        }
        System.out.println(Arrays.toString(text));
    }

    public static void toUpper(char text[]) {
        int left = 'a';
        int right = 'z';
        for (int i = 0; i < text.length; i++) {
            if (text[i] >= left && text[i] <= right) {
                text[i] = (char) (text[i] - 32);
            }
        }
        System.out.println(Arrays.toString(text));
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char text[];
        text = scanner.nextLine().toCharArray();
        int x;
        while (true) {
            System.out.println("Меню:");
            System.out.println("Ваш массив:");
            System.out.println(Arrays.toString(text));
            System.out.println("1. Заменить все большие буквы на маленькие.");
            System.out.println("2. Заменить все маленькие буквы на большие.");
            System.out.println("3. Выход");
            x = scanner.nextInt();
            switch (x) {
                case 1: {
                    toLower(text);
                } break;
                case 2: {
                    toUpper(text);
                } break;
                case 3: {
                    System.exit(0);
                }
                default: {
                    System.out.println("Неверное значение!");
                } break;
            }
        }
    }
}