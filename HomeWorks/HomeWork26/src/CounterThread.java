import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CounterThread extends Thread {
    private int start;
    private int finish;
    private int arr[];
    public int sum;
    private static final Lock lock = new ReentrantLock();
     private static final Object MUTEX = new Object();
    public CounterThread(int start, int finish, int[] arr) {
        this.start = start;
        this.finish = finish;
        this.arr = arr;
        this.sum = 0;
    }

    public int getSum() {
        return sum;
    }

    @Override
    public void run() {
        lock.lock();
//        synchronized (MUTEX) {
            for (int i = start; i <= finish; i++) {
                sum += arr[i];
            }
            Main.mainSum += sum;
//        }
        lock.unlock();
    }
}
