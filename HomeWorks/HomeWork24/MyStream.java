import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class MyStream<T>{
    MyLinkedList<T> linkedList;

    public MyStream(MyLinkedList<T> list) {
        this.linkedList = list;
    }

    public MyStream() {
        linkedList = new MyLinkedList<>();
    }

    public MyStream<T> filter(Predicate<T> predicate) {
        MyStream<T> tempStream = new MyStream<>();
        Iterator<T> iterator = linkedList.iterator();
        while (iterator.hasNext()) {
            T tempValue = iterator.next();
            if (predicate.test(tempValue)) {
                tempStream.linkedList.add(tempValue);
            }
        }
        return tempStream;
    }

    public <R> MyStream<R> map(Function<T, R> function) {
        MyStream<R> tempStream = new MyStream<>();
        Iterator<T> iterator = linkedList.iterator();
        while (iterator.hasNext()) {
            T tempValue = iterator.next();
            tempStream.linkedList.add(function.apply(tempValue));
        }
        return tempStream;
    }

    public void forEach(Consumer<T> consumer) {
        Iterator<T> iterator = linkedList.iterator();
        while (iterator.hasNext()) {
            consumer.accept(iterator.next());
        }
    }

}
