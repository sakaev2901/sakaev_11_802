package com.company;

public class HashMap<K, V> implements Map<K, V>{

    private static class Node<K, V> {
        private K key;
        private V value;
        private Node<K, V> next;
        private int hash;

        private Node(K key, V value) {
            this.key = key;
            this.value = value;
            this.hash = key.hashCode();
        }

        public void setNext(Node<K, V> next) {
            this.next = next;
        }

        public Node<K, V> getNext() {
            return next;
        }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        private void setValue(V value) {
            this.value = value;
        }


    }

    private Node<K, V>[]  nodes;
    private int countOfCollision;
    private HashMap() {
        countOfCollision = 0;
        nodes = new Node[16];
    }

    public int getIndex(K key) {
        int index = key.hashCode() & (nodes.length - 1);
        return index;
    }

    @Override
    public void put(K key, V value) {
        int index = getIndex(key);
        if (nodes[index] == null) {
            nodes[index] = new Node<>(key, value);
        } else {
            countOfCollision++;
            Node<K, V> temp = nodes[index];
            if (key.hashCode() == temp.hash) {
                temp.setValue(value);
                return;
            }
            while (temp.next != null) {
                if (key.hashCode() == temp.hash) {
                    temp.setValue(value);
                    return;
                }
                temp = temp.next;
            }

            temp.next = new Node<>(key, value);
        }
    }
    public int getCountOfCollision() {
        return countOfCollision;
    }
    @Override
    public V get(K key) {
        int index = getIndex(key);
        Node<K, V> temp = nodes[index];
        while (temp != null) {
            if (key.hashCode() == temp.hash) {
                return temp.value;
            }
            temp = temp.next;
        }
        return  null;
    }
}
