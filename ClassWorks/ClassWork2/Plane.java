public class Plane extends Transport implements EditFuel, RepaintCar {
    private int countOfPassenger;
    private String color;

    public Plane(double fuelTank, double capacity, int countOfPassenger, String color) {
        super(fuelTank, capacity);
        this.countOfPassenger = countOfPassenger;
        this.color = color;
    }

    public int getCountOfPassenger() {
        return countOfPassenger;
    }

    public void setCountOfPassenger(int countOfPassenger) {
        this.countOfPassenger = countOfPassenger;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void reduce() {
        this.fuelTank -= 100;
    }

    @Override
    public void addFuel() {
        this.fuelTank += 200;
    }

    @Override
    public void printFuel() {
        System.out.println(this.fuelTank);
    }

    @Override
    public void repaintToBlack() {
        this.color = "black";
    }

    @Override
    public void repaintToRed() {
        this.color = "red";
    }
}
