public class Bike extends Transport implements EditFuel, RepaintCar {
    private int countOfWheel;
    private int countOfSpeeds;
    private String color;

    public Bike(double fuelTank, double capacity, int countOfWheel, int countOfSpeeds, String color) {
        super(fuelTank, capacity);
        this.countOfWheel = countOfWheel;
        this.countOfSpeeds = countOfSpeeds;
        this.color = color;
    }

    public int getCountOfWheel() {
        return countOfWheel;
    }

    public void setCountOfWheel(int countOfWheel) {
        this.countOfWheel = countOfWheel;
    }

    public int getCountOfSpeeds() {
        return countOfSpeeds;
    }

    public void setCountOfSpeeds(int countOfSpeeds) {
        this.countOfSpeeds = countOfSpeeds;
    }

    @Override
    public void reduce() {
        this.fuelTank -= 10;
    }

    @Override
    public void addFuel() {
        this.fuelTank += 20;
    }

    @Override
    public void printFuel() {
        System.out.println(this.fuelTank);
    }

    @Override
    public void repaintToBlack() {
        this.color = "black";
    }

    @Override
    public void repaintToRed() {
        this.color = "red";
    }
}
