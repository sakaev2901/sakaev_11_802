public class ParkingPlace {
    private final int MAX_PARKING_SIZE = 5;
    private int count;

    private Transport transports[];

    public ParkingPlace() {
        this.transports = new Transport[MAX_PARKING_SIZE];
        this.count = 0;
    }

    public void addTransport(Transport transport) {
        if (count < transports.length) {
            transports[count++] = transport;
            count++;
        }
    }

    public void goFromParkingPlace(Transport transport) {
        for (int i = 0; i < this.count; i++) {
            if (transports[i].getRegistrationNumber().equals(transport.getRegistrationNumber())) {
                transports[i] = null;
            }
        }
    }
}
