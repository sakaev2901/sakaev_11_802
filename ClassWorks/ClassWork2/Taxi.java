public class Taxi extends AbstractCivilCar {
    public Taxi(double oilAmount, String registrationNumber) {
        super(oilAmount, registrationNumber);
    }

    @Override
    public void addPassenger(Passenger passenger) {

    }

    @Override
    public void removePassenger(Passenger passenger) {

    }

    @Override
    public int countOfPassenger() {
        return 0;
    }
}
