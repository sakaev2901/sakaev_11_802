public class Bus extends AbstractCivilCar {
    public Bus(double oilAmount, String registrationNumber) {
        super(oilAmount, registrationNumber);
    }

    @Override
    public void addPassenger(Passenger passenger) {

    }

    @Override
    public void removePassenger(Passenger passenger) {

    }

    @Override
    public int countOfPassenger() {
        return 0;
    }
}
