public class Truck extends Transport implements EditFuel, UnloadCargo{
    protected double cargoWeight;
    private int countOfTrailer;

    public Truck(double fuelTank, double capacity, double cargoWeight, int countOfTrailer) {
        super(fuelTank, capacity);
        this.cargoWeight = cargoWeight;
        this.countOfTrailer = countOfTrailer;
    }

    public double getCargoWeight() {
        return cargoWeight;
    }

    public void setCargoWeight(double cargoWeight) {
        this.cargoWeight = cargoWeight;
    }

    public int getCountOfTrailer() {
        return countOfTrailer;
    }

    public void setCountOfTrailer(int countOfTrailer) {
        this.countOfTrailer = countOfTrailer;
    }

    @Override
    public void reduce() {
        this.fuelTank -= 30;
    }

    @Override
    public void addFuel() {
        this.fuelTank += 50;
    }

    @Override
    public void printFuel() {
        System.out.println(this.fuelTank);
    }

}
