public interface CivilTransport {
    void addPassenger(Passenger passenger);
    void removePassenger(Passenger passenger);
    int countOfPassenger();
}
