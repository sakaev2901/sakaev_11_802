public interface MilitaryTransport {
    void addAmmunition(Ammunition ammunition);
    int countAmmunition();
}
