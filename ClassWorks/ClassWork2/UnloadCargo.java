public interface UnloadCargo {
    default void unload(double cargo) {
        cargo = 0;
    }
}
