public class Fraction {
    private int numerator;
    private int b;

    public Fraction() {
        this.numerator = 0;
        this.b = 1;
    }

    public Fraction(int numerator, int b) {
        this.setNumerator(numerator);
        this.setB(b);
    }

    public void setB(int b) {
        if (b > 0) {
            this.b = b;
        } else this.b = 1;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getB() {
        return b;
    }

    public void print() {
        System.out.println(numerator + "/" + b);
    }

    public void optimize() {
        int temp = gcd(this.numerator, this.b);
        this.numerator = this.numerator / temp;
        this.b = this.b / temp;
    }

    private static int gcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

    public void mult(Fraction d1, Fraction d2) {
        this.numerator = d1.numerator * d2.numerator;
        this.b = d1.b * d2.b;
    }
}
