public class ComplexNumber extends Number {
    protected double b;

    public ComplexNumber(double a, double b) {
        super(a);
        this.b = b;
    }

    public void calculateAbs() {
        this.abs = Math.sqrt(a * a + b * b);
    }

    public void printNumb() {
        System.out.println(a + " + " + b + " * i");
    }

    @Override
    public void printAbs() {
        super.printAbs();
    }
}
